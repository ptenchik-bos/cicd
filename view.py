from model import Model

import tkinter as tk
from tkinter import ttk

class View(tk.Tk):
    
    PAD=10
    PADx=80

    def main(self):
      self.mainloop()

    def put_frms(self):
        self._make_main_frame()
        self._make_enrty()
        self._make_buttons()


    def refresh(self):
        all_frams=[f for f in self.children]
        for f_n in all_frams:
            self.nametowidget(f_n).destroy()
        self.put_frms()


    def __init__(self,controller):
        super().__init__()
        self.title('Калькулятор Ипотеки')
        self.controller=controller
        self.value_var1=tk.StringVar()
        self.value_var2=tk.StringVar()
        self.value_var3=tk.StringVar()
        self.value_itog=tk.StringVar()
        self._make_main_frame()
        self._make_enrty()
        self._make_buttons()
     

    def _make_main_frame(self):
        label = ttk.Label(text="Введите сумму (руб.)")
        label.pack()
        self.main_frm1=ttk.Frame(self)
        self.main_frm1.pack(padx=self.PADx,pady=self.PAD)

        label = ttk.Label(text="Введите срок (лет)")
        label.pack()
        self.main_frm2=ttk.Frame(self)
        self.main_frm2.pack(padx=self.PADx,pady=self.PAD)
 
        label = ttk.Label(text="Введите ставку (%)")
        label.pack()
        self.main_frm3=ttk.Frame(self)
        self.main_frm3.pack(padx=self.PADx,pady=self.PAD)
        

    def _make_enrty(self):
        ent1= ttk.Entry(self.main_frm1,textvariable=self.value_var1)
        ent1.pack(fill='x')
        ent2= ttk.Entry(self.main_frm2,textvariable=self.value_var2)
        ent2.pack(fill='x')
        ent3= ttk.Entry(self.main_frm3,textvariable=self.value_var3)
        ent3.pack(fill='x')

    def on_button_click(self): 
        self.controller.calk(self.value_var1.get(),self.value_var2.get(),self.value_var3.get())
        self.refresh()
        label = ttk.Label(text=f' Сумма в месяц: {self.value_itog.get()}')
        label.pack(pady=20)
        

    def _make_buttons(self):
        frm=ttk.Frame(self.main_frm3)
        frm.pack()
        btn=ttk.Button(frm,text='Расчёт',command=self.on_button_click)
        btn.pack(fill='x', pady=10)
        
      
 

